import { Component, OnInit } from '@angular/core';
import {Howl, Howler} from 'howler';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    var sound = new Howl({
      src: ['/assets/img/fire.mp3'],
    });
    sound.once('load', () => {
      sound.play();
    });

    sound.on('end', function(){
      sound.play();
    });
  }

}
